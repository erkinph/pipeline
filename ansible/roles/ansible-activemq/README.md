# ansible-activemq
#### Simple role to install activemq single node

## About
This role installs a activemq server, with a single node implementation, without tweaks.

## How to use
- Import in your ansible project as (https://docs.ansible.com/ansible/2.5/user_guide/playbooks_reuse_roles.html)

#### Requirements
- CentOS 7

#### Roles Variables
```sh
# Defines what activemq version will be installed
activemq_version: 5.15.7
```

#### Example Playbook

```sh
- hosts: all
  become: true
  roles:
    -  ansible-activemq
```

#### Developing
- As development environment was used a vagrant box, all files are in **tests/**

#### TODO
- Configure cluster environment

#### References
- http://activemq.apache.org/

#### Author Information
- Gracco Guimaraes