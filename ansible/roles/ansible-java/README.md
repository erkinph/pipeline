# ansible-java
#### This role configures and install java from Oracle or openjdk

## How to use
- Import in your ansible project as (https://docs.ansible.com/ansible/2.5/user_guide/playbooks_reuse_roles.html)

#### Requirements
- CentOS 7

#### Description

##### Packages
- java-openjdk-8
- java-openjdk-7
- java-openjdk-6
- java-oracle-jre-8
- java-oracle-jdk-8

#### Roles Variables
```sh
# Enabling this variable will install java openjdk, disable the other options
java_openjdk: false
# Enabling this variable will install java from oracle, disable the other options
java_oracle: true
# Enabling this varible will install Oracle java JDK version, not needed for openjdk
java_oracle_jdk: false
# Enabling this varible will install Oracle java JRE version, not needed for openjdk
java_oracle_jre: false
# 6,7,8 for openjdk or 8 only for oracle java
java_version: 8
```

#### Example Playbook

```sh
- hosts: all
  become: true
  roles:
    -  ansible-java
```

#### References
- https://www.oracle.com/technetwork/java/javase/downloads/index.html
- https://openjdk.java.net/

#### Author Information
- Gracco Guimaraes