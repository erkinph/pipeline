# ansible-tomcat
#### Simple role to install tomcat 8 and 9

## About
This role installs a tomcat 8 or 9, without any tweaks. ( This role doesn't install java, use ansible-java)

## How to use
- Import in your ansible project as (https://docs.ansible.com/ansible/2.5/user_guide/playbooks_reuse_roles.html)

#### Requirements
- CentOS 7

#### Roles Variables
```sh
# Defines what tomcat MAIN version will be installed
tomcat_main_version: 8
# Defines what tomcat FULL version will be installed
# https://tomcat.apache.org/download-80.cgi
# https://tomcat.apache.org/download-90.cgi
tomcat_full_version: 8.5.35
```

#### Example Playbook

```sh
- hosts: all
  become: true
  roles:
    -  ansible-tomcat
```

#### Developing
- As development environment was used a vagrant box, all files are in **tests/**

#### TODO
- Configure some JVM tunnings

#### References
-  https://tomcat.apache.org/download-80.cgi
- https://tomcat.apache.org/download-90.cgi

#### Author Information
- Gracco Guimaraes