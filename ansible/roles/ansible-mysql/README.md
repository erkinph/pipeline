# ansible-mysql
#### Simple role to install Oracle mysql

## About
This role installs a basic Mysql Instance, without any tweaks.

## How to use
- Import in your ansible project as (https://docs.ansible.com/ansible/2.5/user_guide/playbooks_reuse_roles.html)

#### Requirements
- CentOS 7

#### Roles Variables
```sh
# Defines root mysql password
mysql_root_password: "Your password, try to use ansible-vault"

```

#### Example Playbook

```sh
- hosts: all
  become: true
  roles:
    -  ansible-mysql
```

#### Developing
- As development environment was used a vagrant box, all files are in **tests/**

#### TODO
- Configure some Mysql tunnings

#### References
- https://stackoverflow.com/questions/25136498/ansible-answers-to-mysql-secure-installation
- https://docs.ansible.com/ansible/latest/modules/mysql_user_module.html


#### Author Information
- Gracco Guimaraes