# ansible-common
#### This role configures and install common packages

## How to use
- Import in your ansible project as (https://docs.ansible.com/ansible/2.5/user_guide/playbooks_reuse_roles.html)

#### Requirements
- CentOS 7

#### Description

##### Packages
- htop
- vim

##### Configuration
- Disable SELINUX
- Configure hostname on AWS

#### Roles Variables
```sh
# Enabling this variable, the role will configured the hostname instance as the tag Name, use this only for AWS environments
set_hostname: false
# Enabling this variable, the role will configure the SELINUX as disabled
disable_selinux: true
```

#### Example Playbook

```sh
- hosts: all
  become: true
  roles:
    -  ansible-common
```

#### References

#### Author Information
- Gracco Guimaraes