# Ansible project for cx6 cookbook
##### This is ansible repository that correspond to an actual environment.

#### Environments
- dev


#### Running
- $ ansible-playbook -i [environment] site.yml

#### TODO
- Create all variables ( tomcat ports, paths)
- Enable vault for passwords and secrets
- Organize better the mysql connector
- Test in separated vms
- Think in a better way to uncouple multiple-tomcat instalations ( change the tomcat-role, to use named tomcats instalations )
- Create a dummy user to run the springboot projects ( now is running mock service as root)
- Create a deployment playbook for IPS, CX6
- Create a import statics playbook

#### Referencies
- Ansible Best Practices - https://docs.ansible.com/ansible/latest/user_guide/playbooks_best_practices.html
- Ansible Vault - https://docs.ansible.com/ansible/latest/user_guide/vault.html#ansible-vault
